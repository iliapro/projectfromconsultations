import './App.css'
import {Redirect, Route} from "react-router-dom"
import Main from "../Main/Main"

function App() {
    return (
        <div className="App">
            <Route path={"/:lang"} component={Main}/>
            <Route exact path={"/"}>
                <Redirect to={"/ru"}/>
            </Route>
        </div>
    )
}

export default App